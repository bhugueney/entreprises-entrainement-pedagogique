# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: "config.org"

#+TITLE: Entreprise d’Entraînement Pédagogique
#+AUTHOR: Mosbah Ejebali & Bernard Hugueney
#+DATE: Formation de formateurs 2019-02-18 \rightarrow 2019-02-22

* Meta

URL: https://huit.re/G_3EQf79
#+REVEAL_HTML: <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGMAAABjAQAAAACnQIM4AAAA6UlEQVQ4jc3UsY3EIBAF0EEEhNuA%0AJdogoyVowBwNrFsiow0kGjAZAWIWn2R5E3asC047ES8Y8QcQgO8FX6wdwMWMARilgm3VjcWxoBQW%0AW3GLYG9o1fK+8I6wuSD9lWyqMZ+Ni72mnWpUqcleJzjVLkaH/BHN0cpdcTyzfJJqjyC7WBglrLmP%0ATarcKHWVHuHYx1MafT4kI3KhVGIyutnaGKWuZAnJ4ZllrhLBId+BI6Xfe+AFuad03G3gPtIa78WI%0Ao28jFRYDYM5kn7WqxCq4GzI6P7UspEaW0feWbKb9CJJsQE/pv3+iv+kFdQnAB1ATnkIAAAAASUVO%0ARK5CYII="  height=400 width=400 >

[[https://gitlab.com/bhugueney/entreprises-entrainement-pedagogique/edit/master/howto.org][Link to edit this presentation]] (you might need to [[https://docs.gitlab.com/ee/user/project/members/#request-access-to-a-project][request access]])

[[https://gitlab.com/bhugueney/entreprises-entrainement-pedagogique/issues][Link to post requests]]

[[https://bhugueney.gitlab.io/entreprises-entrainement-pedagogique/Entreprises-Entrainement-Pedagogie.html?print-pdf][Link to printable version]] (append ~?print-pdf~ to the URL)

(slides made according to [[https://oer.gitlab.io/emacs-reveal-howto/howto.html][emacs-reveal-howto]] from Jens Lechtenbörger )

#+BEGIN_NOTES
Add speaker notes everywhere ! ☺
#+END_NOTES

* Principles
Play not as in game, but as in "play fight" \rightarrow live action
role playing game

** As close as possible to a real company

- everything except for actual production (& procurement and sale
  departments) : no money involved
- 
- actual businesses as partners ("actual" paperwork)

#+BEGIN_NOTES
Add speaker notes here too ! ☺
#+END_NOTES

* Origins
From Germany starting 1870 ramping up after First World War (soldiers
from farms to city jobs) \rightarrow France 1989.
* Implementations
A hundred EEP in France
** Caracteristics
Each EEP trains 10-15 interns for 4 months

** Enrollement statistics
\(\simeq\) 7000 persons per year :
- 80% Stages d’Insertion et de Formation à l’Emploi \rightarrow >26 ans
- 85% of young interns are women
- Academic level :
  - at least CEP/ BEPC
  - most (70%) CAP/BEP or baccalauréat 
  - \(\simeq 10%\) first years of College degree ("1er cycle")

** Goals

*** Skills
- (5 or 6) technical skills («compétences techniques») with 4 levels
  each (target proficiency is level 3)
- behavioral profiles «profil comportemental» :
  - communication
  - self-confidence
  - receptivity
  - autonomy
  - self-control

assessment and self-assessment

*** Employability
Hiring statistics

\rightarrow 70% of interns get hired after the internship


  
** Sub categories
Different emphasis on academic vs workplace learning
- EEP : balanced
- EE : workplace
- Academic EE : prior academic experience

[[https://www.euroentent.net/CarteFrance.aspx][All over France]] : over a hundred, 90% lifelong learning, 1 for inmates.



* Trends
At least since 2004, the number of EEP is declining.


* Assessment
If the number of EEP is declining, it is because the EEP did not
convince the funding agencies
** Learners Assessments
skills vs hireability
** EEP Assessments
Return On Investment of the hireability impact
** Pedagogy Assessments
Return On Investment of the impact correcting for selection
bias

Cf.
 [[http://eprints.whiterose.ac.uk/141754/][Meta analysis of educational randomized controlled trials]]


Rigorous assessment is required because for poorly understood complex
systems (like humans !) [[http://journals.sagepub.com.sci-hub.tw/doi/10.1177/0002716202250781][good intentions are not enough]].

* Selection
[[https://fredrikdeboer.com/2017/03/29/why-selection-bias-is-the-most-powerful-force-in-education/][Selection cannot be overlook in education]]
** EEP Enrollment
EEP assessment drive selection trends in enrollment \rightarrow
increasing the academic background requirements

** The selection dilemma
cf. Feynman on the teacher's curse :
/Those we can help the most are those who need us the least/

* Bibliography

- [[https://www.cairn.info/revue-education-et-societes-2001-1-page-67.htm][Les entreprises d'entraînement: logique formative ou logique productive ?]] Cédric FRÉTIGNÉ Éducation et Société № 7/2001/1 pp.67-80

- [[https://www.forpro-creteil.org/upload/files/GMTE93/107_ALECOUTE_FOIRE%2520EEP.pdf][Une fausse foire pour une vrai métier]] Marlène LENFANT d'après un
  article paru dans le JSD (journal de Saint-Denis) publié le
  <2016-04-11 Mon>
- [[https://www.liberation.fr/futurs/2000/06/19/l-entreprise-comme-si-vous-y-etiez_327770][L'entreprise comme si vous y étiez]] Hervé DIEU et Corinne HYAFIL
  Libération <2000-06-19 Mon>
- [[https://www.persee.fr/doc/forem_0759-6340_2004_num_88_1_1738][Les enjeux de la mesure des acquis en entreprise d'entrainement]]
  Cédric FRÉTIGNÉ 2004 Formation Emploi 88 pp. 57-69


* TODO Further References
- [[http://www.theses.fr/2001PA100138][Les entreprises d'entraînement : entre organisations formatives et
  organisations productives]] Cédric Frétigné, 2001, Thèse de doctorat
  en Sociologie Université Paris 10, 322 p.
- [[http://www.editions-harmattan.fr/index.asp?navig=catalogue&obj=livre&no=20637][Les Entreprises d'Entraînement ou Pédagogiques]] Pierre Troton 2006,
  L'Harmattan, 196 p.


* Thank you for your attention !
Question ?

#+MACRO: copyrightyears 2017, 2018, 2019
#+INCLUDE: "emacs-reveal/org/license-template.org"

# Local Variables:
# indent-tabs-mode: nil
# End:
